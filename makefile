login:
	eval $(shell aws ecr get-login --no-include-email --region us-east-2)
build: login
	docker build -t onepipe_demo_provider .
tag: build
	docker tag onepipe_demo_provider:latest 521520363775.dkr.ecr.us-east-2.amazonaws.com/onepipe_demo_provider:${BITBUCKET_BUILD_NUMBER}
push: tag
	docker push 521520363775.dkr.ecr.us-east-2.amazonaws.com/onepipe_demo_provider:${BITBUCKET_BUILD_NUMBER}
deploy: login
	docker run -i -p 80:3000 -e AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID} -e AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY} -e AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION} 521520363775.dkr.ecr.us-east-2.amazonaws.com/eshu:latest deploy onepipe_demo_provider ${ENV} ${BITBUCKET_BUILD_NUMBER}