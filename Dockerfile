FROM node:10-alpine as build
WORKDIR /app
COPY package.json /app
# RUN npm config set unsafe-perm true
# RUN npm help config
# RUN npm install && npm audit fix
RUN npm install
RUN npm install sequelize-cli -g
COPY . /app
EXPOSE 3000
RUN ["chmod", "+x", "/app/deploy.sh"]
ENTRYPOINT ["/app/deploy.sh"]