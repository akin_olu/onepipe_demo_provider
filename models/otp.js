'use strict';
module.exports = (sequelize, DataTypes) => {
  const OTP = sequelize.define('OTP', {
    otp: DataTypes.INTEGER,
    expiry_date: DataTypes.DATE,
    used: DataTypes.BOOLEAN,
    transaction_ref: DataTypes.STRING,
    app_name: DataTypes.STRING
  }, {});
  OTP.associate = function(models) {
    // associations can be defined here
  };
  return OTP;
};