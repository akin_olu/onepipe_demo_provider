const app = require('express')();
let bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json());
require('dotenv').config()
let listener = require('./services/listener')
let sendOTP = require('./services/sendOTP/sendOTP')
let collect = require('./services/collect/collect')
let validateOTP = require('./services/validateOTP/validateOTP')





const PORT = process.env.PORT || 80;


app.get('/health',(req,res)=>{
    res.json({
        "status":200,
        "message": "Oneline....."
    })
})
app.use('/inspect',listener)
app.use('/sendotp',sendOTP)
app.use('/validateotp',validateOTP)
app.use('/collect',collect)







app.listen(process.env.PORT,()=>{
    console.log(`Listening on..........${PORT}`)
})