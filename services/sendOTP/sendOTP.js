const { encrypt, decrypt } = require('../../onepipe-crypto');
const moment = require('moment');
const axios = require('axios');
const otp = require("../../helper/generateOTP");
let encryptedResponse, responseData, formatted_response_type;
let Router = require('express').Router();
let decode = require('../..//helper/decodePayload');
let otpModel = require('../../models').OTP
Router.post('/', async(req, res) => {
  console.log("moment.....")
  console.log(moment().add(5,"M").toISOString())
  let incomingPayload = decode(`${process.env.enc_key}`, req.body.data);
  request_type = incomingPayload.request_type
  formatted_response_type = request_type.split(" ");
  formatted_response_type = formatted_response_type.join("");
//   console.log(`Format: ${formatted_response_type}`);
  console.log(incomingPayload)
headers = {
  "Content-Type": "application/json",
  "Authorization": "bearer NPndYjsExa6AFh1YnjIkkk32GNcpqybUYeuXfABYHtUJnb92jeAo3"
}
let otpToBeDelivered = otp();
body= {
  "senderName":`Sample`,
  // "senderName":`${incomingPayload.transaction.app_info.name}`,
	// "recipientPhoneNumber":"sssss",
	"recipientPhoneNumber":incomingPayload.transaction.customer.customer_ref,
	"message":`${otpToBeDelivered}`
}
  responseData = {
    "status": "Successful",
    "message": "Please enter the OTP sent to 234*****4656",
    "data": {
        "provider_response_code": "00",
        "provider": "Demoprovider",
        "errors": null,
        "error": null,
        "provider_response": {
            "delivery_status": "delivered",
            "cost": "0",
            "expires_on": null,
            "ussd_code": null,
            "prompt_message": null,
            "reference": null,
            "meta":{
              
            }
        }
    }
}
  // console.log("................")
  // console.log(otp())
  // console.log(incomingPayload.transaction.customer.customer_ref)
  // console.log(incomingPayload)
try {
  let expiry_date = moment().add(5,"minute").toISOString();
  let sendSMS = await axios.post(`${process.env.smsURL}`, body, {
    // timeout: 2000,
    headers
})
///////////
if(sendSMS.data.code === "99"){
  throw sendSMS.data.message;
}
responseData.data.provider_response.expires_on = expiry_date;
responseData.data.provider_response.prompt_message = "Enter OTP sent to ...."
  let sendOTP = await otpModel.create({
    otp: otpToBeDelivered,
    expiry_date: expiry_date,
    transaction_ref: incomingPayload.transaction.transaction_ref,
    app_name: incomingPayload.transaction.app_info.name
  })
  // console.log(sendOTP)
  
// console.log("successful")
// console.log(sendSMS.data)
} catch (error) {
  console.log("error");
  console.log(error)
  responseData.status = "Failed";
  responseData.message = error;
  responseData.data.delivery_status = "failed";
  responseData.data.prompt_message = "ERROR!!"

}
  encryptedResponse = encrypt(
    `${process.env.enc_key}`,
    JSON.stringify(responseData)
  );
  res.json({
    data: encryptedResponse
  });
});
module.exports = Router;