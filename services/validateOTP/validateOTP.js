const { encrypt, decrypt } = require('../../onepipe-crypto');
const moment = require('moment');
const axios = require('axios');
let encryptedResponse, responseData, formatted_response_type, updateOTP;
let Router = require('express').Router();
let decode = require('../../helper/decodePayload');
let otpModel = require('../../models').OTP
Router.post('/', async(req, res) => {
  let incomingPayload = decode(`${process.env.enc_key}`, req.body.data);
  console.log(incomingPayload)
  responseData = {
    "status": "Successful",
    "message": "OTP validated correctly",
    "data": {
        "provider_response_code": "00",
        "provider": "Demoprovider",
        "errors": null,
        "error": null,
        "provider_response": {
            "cost": "0",
            "reference": "",
            "meta":{
              
            }
        }
    }
}
 
try {
  console.log("////////////////////////////////////////////////////////")
  console.log(`${incomingPayload.transaction.transaction_ref}`)
  let getOTP = await otpModel
      .findAll({
          where: {
            transaction_ref: `${incomingPayload.transaction.details.reference}`,
            // otp: incomingPayload.transaction.details.otp,
            app_name: incomingPayload.transaction.app_info.name,
          },
          raw: true
        }) 
        if(getOTP.length === 0){
          // console.log("No record")
          throw "Transaction is not awaiting token validation"
        }
        getOTP = await otpModel
      .findAll({
          where: {
            transaction_ref: `${incomingPayload.transaction.details.reference}`,
            otp: incomingPayload.transaction.details.otp,
            app_name: incomingPayload.transaction.app_info.name,
          },
          raw: true
        }) 
        if(getOTP.length === 0){
          // console.log("No record")
          throw "Invalid OTP, please provide the correct OTP"
        }
        let isexpired = moment(moment().toISOString()).isSameOrAfter(getOTP[0].expiry_date)
        // console.log(moment().toISOString())
        console.log(isexpired)
        if(getOTP[0].used === 1){
          throw "OTP not awaiting validation"
        }
        if(!isexpired){
          updateOTP = await otpModel
      .update({
          used: true
        },
        {where:{transaction_ref: `${incomingPayload.transaction.details.reference}`,
        otp: incomingPayload.transaction.details.otp,
        app_name: incomingPayload.transaction.app_info.name,}}
        )
        }else{
          throw "OTP expired"
        }
        console.log("......................................")
        console.log(updateOTP)
 
} catch (error) {
  console.log("error");
  console.log(error)
  responseData.status = "Failed";
  responseData.message = error;
  responseData.data.delivery_status = "failed";
  responseData.data.prompt_message = "ERROR!!"

}
  encryptedResponse = encrypt(
    `${process.env.enc_key}`,
    JSON.stringify(responseData)
  );
  res.json({
    data: encryptedResponse
  });
});
module.exports = Router;