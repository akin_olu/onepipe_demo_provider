const { encrypt, decrypt } = require('../onepipe-crypto');
let encryptedResponse, responseData, request_type, formatted_response_type;
let Router = require('express').Router();
let decode = require('../helper/decodePayload');
Router.post('/', (req, res) => {
  let incomingPayload = decode(`${process.env.enc_key}`, req.body.data);
  console.log("///..................................//////////////////////")
  console.log(incomingPayload.auth.auth_provider)
  request_type = incomingPayload.request_type
  formatted_response_type = request_type.split(" ");
  formatted_response_type = formatted_response_type.join("");
  console.log(`Format: ${formatted_response_type}`);
  console.log(incomingPayload)
  responseData = {}
  switch (request_type) {
    case "collect":
      console.log("I'm calling collect")
      responseData = {
        "status": "Successful",
        "message": "Transaction processed successfully",
        "data": {
          "provider_response_code": "00",
          "provider": "DemoProvider",
          "errors": null,
          "error": null,
          "provider_response": {
            "provider_auth_token": null,
            "paymentoptions": [],
            "transaction_final_amount": 1000,
            "reference": "897465522772655",
            "meta": {}
          },
          "client_info": {
            "name": null,
            "id": null,
            "bank_cbn_code": null,
            "bank_name": null,
            "console_url": null,
            "js_background_image": null,
            "css_url": null,
            "logo_url": null,
            "footer_text": null,
            "show_options_icon": false,
            "paginate": false,
            "paginate_count": 0,
            "options": null,
            "merchant": null,
            "colors": null,
            "meta": null
          }
        }
      }
      break;
    case "lookup bvn min":
      console.log("lookup bvn min")
      responseData = {
        "status": "Successful",
        "message": "Transaction processed successfully",
        "data": {
          "provider_response_code": "00",
          "provider": `${incomingPayload.auth.auth_provider}`,
          "errors": null,
          "error": null,
          "provider_response": {
            "bvn": "22333055555",
            "first_name": true,
            "last_name": true,
            "dob": true,
            "reference": "98776665556665",
            "meta": {}
          }
        }
      }
      break;
    case "lookup bvn mid":
      console.log("lookup bvn mid")
      responseData = {
        "status": "Successful",
        "message": "Transaction processed successfully",
        "data": {
          "provider_response_code": "00",
          "provider": `${incomingPayload.auth.auth_provider}`,
          "errors": null,
          "error": null,
          "provider_response": {
            "reference": "98776665556665",
            "bvn": "22333055555",
            "first_name": "John",
            "middle_name": "Jane",
            "last_name": "Doe",
            "dob": "1990-12-22",
            "phone_number1": "07037608648",
            "phone_number2": "",
            "registration_date": "1990-12-22",
            "email": "john.doe@gmail.com",
            "gender": "Male",
            "level_of_account": "Level 2 - Medium Level Accounts",
            "lga_of_origin": "Owerri West",
            "lga_of_residence": "Ikeja",
            "marital_status": "Single",
            "meta": {}
          }
        }
      }
      break;
    case "lookup bvn max":
      console.log("lookup bvn max")
      responseData = {
        "status": "Successful",
        "message": "Transaction processed successfully",
        "data": {
          "provider_response_code": "00",
          "provider": `${incomingPayload.auth.auth_provider}`,
          "errors": null,
          "error": null,
          "provider_response": {
            "reference": "98776665556665",
            "bvn": "22333055555",
            "first_name": "John",
            "middle_name": "Jane",
            "last_name": "Doe",
            "dob": "1990-12-22",
            "phone_number1": "07037608648",
            "phone_number2": "",
            "registration_date": "1990-12-22",
            "enrollment_bank": "011",
            "enrollment_branch": "MATORI",
            "email": "john.doe@gmail.com",
            "gender": "Male",
            "level_of_account": "Level 2 - Medium Level Accounts",
            "lga_of_origin": "Owerri West",
            "lga_of_residence": "Ikeja",
            "marital_status": "Single",
            "nin": "77766492344",
            "name_on_card": "John Jane Doe",
            "nationality": "Nigeria",
            "residential_address": "34, Banjo Street, Ikeja, Lagos",
            "state_of_origin": "Imo State",
            "state_of_residence": "Lagos State",
            "title": "Mr",
            "watchlisted": "NO",
            "base64_image": "{{Base64Image}}",
            "meta": {}
          }
        }
      }
      break;
    case "open account":
      console.log("open account")
      responseData = {
        "status": "Successful",
        "message": "Transaction processed successfully",
        "data": {
          "provider_response_code": "00",
          "provider": `${incomingPayload.auth.auth_provider}`,
          "errors": null,
          "error": null,
          "provider_response": {
            "reference": "263636363633777",
            "account_number": "2233305555",
            "contract_code": null,
            "account_reference": "48214462006092",
            "account_name": "Akinkunmi Olunloye",
            "currency_code": "NGN",
            "customer_email": "akin@onepipe.io",
            "bank_name": null,
            "bank_code": null,
            "account_type": null,
            "status": "ACTIVE",
            "created_on": "2020-04-05 23:08:01",
            "meta": {}
          }
        }
      }
      break;
    case "disburse":
      console.log("disburse")
      responseData = {
        "status": "Successful",
        "message": "Transaction processed successfully",
        "data": {
          "provider_response_code": "00",
          "provider": `${incomingPayload.auth.auth_provider}`,
          "errors": null,
          "error": null,
          "provider_response": {
            "reference": "000022200225154318222333334432",
            "payment_id": "136FTTP200590171",
            "destination_institution_code": "000016",
            "beneficiary_account_name": "JOHN DOE JAMES",
            "beneficiary_account_number": "3056433222",
            "beneficiary_kyc_level": "3",
            "originator_account_name": "James Jane",
            "originator_account_number": "0001131256",
            "originator_kyc_level": "1",
            "narration": "My narration",
            "transaction_final_amount": 1000,
            "meta": {}
          }
        }
      }
      break;
    case "transfer funds":
      console.log("transfer funds")
      responseData = {
        "status": "Successful",
        "message": "Transaction processed successfully",
        "data": {
          "provider_response_code": "00",
          "provider": `${incomingPayload.auth.auth_provider}`,
          "errors": null,
          "error": null,
          "provider_response": {
            "destination_institution_code": "000016",
            "beneficiary_account_name": "JOHN DOE JAMES",
            "beneficiary_account_number": "3056433222",
            "beneficiary_kyc_level": "3",
            "originator_account_name": "James Jane",
            "originator_account_number": "0001131256",
            "originator_kyc_level": "1",
            "narration": "My narration",
            "transaction_final_amount": 1000,
            "reference": "000022200225154318222333334432",
            "meta": {
              "field_key": "field_value",
            }
          }
        }
      }
      break;
    case "lookup nuban":
      console.log("lookup nuban")
      responseData = {
        "status": "Successful",
        "message": "Transaction processed successfully",
        "data": {
          "provider_response_code": "00",
          "provider": `${incomingPayload.auth.auth_provider}`,
          "errors": null,
          "error": null,
          "provider_response": {
            "banks": [
              {
                "bank_code": "050",
                "bank_name": "Ecobank Bank"
              },
              {
                "bank_code": "214",
                "bank_name": "FCMB"
              },
              {
                "bank_code": "313",
                "bank_name": "Mkudi"
              },
              {
                "bank_code": "401",
                "bank_name": "ASOSavings"
              },
              {
                "bank_code": "917",
                "bank_name": "GOWANS MFB"
              }
            ],
            "reference": "000022200225154318222333334432",
            "meta": {

            }
          }
        }
      }
      break;
    case "get banks":
      console.log("get banks")
      responseData = {
        "status": "Successful",
        "message": "Transaction processed successfully",
        "data": {
          "provider_response_code": "00",
          "provider": `${incomingPayload.auth.auth_provider}`,
          "errors": null,
          "error": null,
          "provider_response": {
            "banks": [
              {
                "bank_code": "050",
                "bank_name": "Ecobank Bank"
              },
              {
                "bank_code": "214",
                "bank_name": "FCMB"
              },
              {
                "bank_code": "313",
                "bank_name": "Mkudi"
              },
              {
                "bank_code": "401",
                "bank_name": "ASOSavings"
              },
              {
                "bank_code": "917",
                "bank_name": "GOWANS MFB"
              }
            ],
            "reference": "000022200225154318222333334432",
            "meta": {
              "field_key": "field_value",
              "field_key": "field_value"
            }
          }
        }
      }
      break;
    case "get balance":
      console.log("get balance")
      responseData = {
        "status": "Successful",
        "message": "Transaction processed successfully",
        "data": {
          "provider_response_code": "00",
          "provider": `${incomingPayload.auth.auth_provider}`,
          "errors": null,
          "error": null,
          "provider_response": {
            "account_id": "50617644717007",
            "account_type": "Verve",
            "account_number": "6200000766",
            "avialable_balance": 2077099,
            "ledger_balance": 2077099,
            "minimum_balance": 0,
            "account_class": "STFCUR",
            "account_status": "ACCOUNT OPEN",
            "currency": "NGN",
            "reference": "000022200225154318222333334432",
            "meta": {
              "field_key": "field_value",
              "field_key": "field_value"
            }
          }
        }
      }
      break;
    case "card issue":
      console.log("card issue")
      responseData = {
        "status": "Successful",
        "message": "Transaction processed successfully",
        "data": {
          "provider_response_code": "00",
          "provider": `${incomingPayload.auth.auth_provider}`,
          "errors": null,
          "error": null,
          "provider_response": {
            "card_no_masked": "506100xxxxxx4998",
            "card_sequence_no": "001",
            "card_brand": "Mastercard",
            "card_scheme_code": "001",
            "card_scheme_name": "Osun Students Card",
            "name_on_card": "Opeyemi O. Adamu",
            "address_line_1": "23 Ebinpejo Lane",
            "address_line_2": "Idumota",
            "address_city": "Lagos",
            "address_state": "Lagos",
            "address_postal_code": "23401",
            "address_country_code": "NGN",
            "card_currency_code": "566",
            "issuer_cbn_code": "43",
            "otp_setup_response": "9000",
            "secure": `TripleDES.encrypt("{card.Pan};{card.Cvv};{card.Expdate};{card.Pin}",secretKey)`,
            "reference": "000022200225154318222333334432",
            "meta": {
              "field_key": "field_value",
              "field_key": "field_value"
            }
          }
        }
      }
      break;
    case "get statement":
      console.log("get statement")
      responseData = {
        "status": "Successful",
        "message": "Transaction processed successfully",
        "data": {
          "provider_response_code": "00",
          "provider": `${incomingPayload.auth.auth_provider}`,
          "errors": null,
          "error": null,
          "provider_response": {
            "closing_balance": 1056109,
            "opening_balance": 186279,
            "statement_list": [
              {
                "transaction_reference": "788FTIB20045GJU",
                "transaction_amount": 3000563,
                "balance": 1056109,
                "transaction_type": "D",
                "transaction_date": "2020-02-14T00:00:00",
                "description": "MOB.BNKG:011/MUSA TOLULOPE/TRANSFER 30000 FROM 1780***243 TO 1077***364:000008200214190749669842598346"
              },
              {
                "transaction_reference": "382FTTP20045000J",
                "transaction_amount": 3000,
                "balance": 4056672,
                "transaction_type": "C",
                "transaction_date": "2020-02-14T00:00:00",
                "description": "MB: trans: Trf from CHIJIOKE JULIET JOHN to JOHN DOE JANE"
              },
              {
                "transaction_reference": "136n15a200450084",
                "transaction_amount": 400,
                "balance": 3756672,
                "transaction_type": "D",
                "transaction_date": "2020-02-14T00:00:00",
                "description": "SMS SERVICE UNCHARGED APPLIED JAN 2020-1780161243"
              }
            ],
            "reference": "000022200225154318222333334432",
            "meta": {
              "field_key": "field_value",
              "field_key": "field_value"
            }
          }
        }
      }
      break;
    case "list branches":
      console.log("list branches")
      responseData = {
        "status": "Successful",
        "message": "Transaction processed successfully",
        "data": {
          "provider_response_code": "00",
          "provider": `${incomingPayload.auth.auth_provider}`,
          "errors": null,
          "error": null,
          "provider_response": {
            "branches": [
              {
                "branch_code": "355",
                "branch_name": "BRD STR BRANCH",
                "branch_address1": "51/55  BROAD STREET, ",
                "branch_address2": "LAGOS",
                "branch_address3": " LAGOS "
              },
              {
                "branch_code": "364",
                "branch_name": "KIRIKASA BRANCH",
                "branch_address1": "1, KIRIKASSAMA  ROAD ",
                "branch_address2": "MAIDUGURI",
                "branch_address3": "BORNO"
              }
            ],
            "reference": "000022200225154318222333334432",
            "meta": {
              "field_key": "field_value",
              "field_key": "field_value"
            }
          }
        }
      }
      break;
    case "send sms":
      console.log("send sms")
      responseData = {
        "status": "Successful",
        "message": "Transaction processed successfully",
        "data": {
          "provider_response_code": "00",
          "provider": `${incomingPayload.auth.auth_provider}`,
          "errors": null,
          "error": null,
          "provider_response": {
            "delivery_status": "delivered",
            "cost": "1200",
            "reference": "000022200225154318222333334432",
            "meta": {
              "field_key": "field_value",
            }
          }
        }
      }
      break;
    case "send email":
      console.log("send email")
      responseData = {
        "status": "Successful",
        "message": "Transaction processed successfully",
        "data": {
          "provider_response_code": "00",
          "provider": `${incomingPayload.auth.auth_provider}`,
          "errors": null,
          "error": null,
          "provider_response": {
            "delivery_status": "delivered | failed | pending",
            "cost": "1200",//in kobo
            "reference": "000022200225154318222333334432",
            "meta": {
              "field_key": "field_value",
              "field_key": "field_value"
            }
          }
        }
      }
      break;
    case "send otp":
      console.log("send otp")
      responseData = {
        "status": "Successful",
        "message": "Please enter the OTP sent to 234*****4656",
        "data": {
          "provider_response_code": "00",
          "provider": `${incomingPayload.auth.auth_provider}`,
          "errors": null,
          "error": null,
          "provider_response": {
            "delivery_status": "delivered | failed | pending | logged",
            "cost": "1200",//in kobo
            "expires_on": "yyyy-MM-dd-hh-mm-ss",
            "ussd_code": "*347*022*2*99312#", //in case of ussd
            "prompt_message": "Please enter the OTP sent to 234*****4656",
            "reference": "000022200225154318222333334432",
            "meta": {
              "field_key": "field_value",
              "field_key": "field_value"
            }
          }
        }
      }
      break;
    case "validate otp":
      console.log("validate otp")
      responseData = {
        "status": "Successful",
        "message": "OTP validated correctly",
        "data": {
          "provider_response_code": "00",
          "provider": `${incomingPayload.auth.auth_provider}`,
          "errors": null,
          "error": null,
          "provider_response": {
            "cost": "1200",
            "reference": "000022200225154318222333334432",
            "meta": {
              "field_key": "field_value",
              "field_key": "field_value"
            }
          }
        }
      }
      break;
    case "buy airtime":
      console.log("buy airtime")
      responseData = {
        "status": "Successful",
        "message": "Transaction processed successfully",
        "data": {
          "provider_response_code": "00",
          "provider": `${incomingPayload.auth.auth_provider}`,
          "errors": null,
          "error": null,
          "provider_response": {
            "payment_status": "Successful",
            "fulfillment_status": "Succesful",
            "transaction_final_amount": 20000,//in kobo
            "transaction_fee": 5000,//in kobo
            "narration": "My narration",
            "reference": "000022200225154318222333334432",
            "meta": {
              "field_key": "field_value",
              "field_key": "field_value"
            }
          }
        }
      }
      break;
    case "buy data":
      console.log("buy data")
      responseData = {
        "status": "Successful",
        "message": "Transaction processed successfully",
        "data": {
          "provider_response_code": "00",
          "provider": `${incomingPayload.auth.auth_provider}`,
          "errors": null,
          "error": null,
          "provider_response": {
            "payment_status": "Successful",
            "fulfillment_status": "Succesful",
            "transaction_final_amount": 20000,//in kobo
            "transaction_fee": 5000,//in kobo
            "narration": "My narration",
            "reference": "000022200225154318222333334432",
            "meta": {
              "field_key": "field_value",
              "field_key": "field_value"
            }
          }
        }
      }
      break;
    case "buy scratch card":
      console.log("buy scratch card")
      responseData = {
        "status": "Successful",
        "message": "Transaction processed successfully",
        "data": {
          "provider_response_code": "00",
          "provider": `${incomingPayload.auth.auth_provider}`,
          "errors": null,
          "error": null,
          "provider_response": {
            "scratch_card_number": "0000999888999",
            "sratch_card_pin": "1234",
            "scratch_card_serial": "988988888ASZ3334",
            "payment_status": "Successful",
            "fulfillment_status": "Succesful",
            "transaction_final_amount": 20000,//in kobo
            "transaction_fee": 5000,//in kobo
            "narration": "My narration",
            "reference": "000022200225154318222333334432",
            "meta": {
              "field_key": "field_value",
              "field_key": "field_value"
            }
          }
        }
      }
      break;
    case "lookup customer reference":
      console.log("lookup customer reference")
      responseData = {
        "status": "Successful",
        "message": "Transaction processed successfully",
        "data": {
          "provider_response_code": "00",
          "provider": `${incomingPayload.auth.auth_provider}`,
          "errors": null,
          "error": null,
          "provider_response": {
            "products": [
              {
                "order_reference": "101638163628490752",
                "biller_item_id": "101638163628490752",
                "biller_item_code": "RAC01",
                "biller_item_name": "DSTV Premium",
                "biller_item_description": "This is DSTV Premium that is lovely",
                "biller_item_image_url": "{{url to an image}}",
                "biller_item_prompt": "Please neter your smartcard number",
                "customer_name": "Kola Uju Abubakar Premium",
                "biller_item_meta": {},
                "amount": "500000",
                "currency": "566",
                "terms": "{{some HTML content}}",
                "terms_url": "{{some url}}"
              },
              {
                "order_reference": "101638163628490752",
                "biller_item_id": "101638163628490752",
                "biller_item_code": "RAC01",
                "biller_item_name": "DSTV Premium",
                "biller_item_description": "This is DSTV Premium that is lovely",
                "biller_item_image_url": "{{url to an image}}",
                "biller_item_prompt": "Please neter your smartcard number",
                "customer_name": "Kola Uju Abubakar Premium",
                "biller_item_meta": {},
                "amount": "500000",
                "currency": "566",
                "terms": "{{some HTML content}}",
                "terms_url": "{{some url}}"
              }
            ],
            "reference": "000022200225154318222333334432",
            "meta": {
              "field_key": "field_value",
              "field_key": "field_value"
            }
          }
        }
      }
      break;
    case "pay tv":
      console.log("pay tv")
      responseData = {
        "status": "Successful",
        "message": "Transaction processed successfully",
        "data": {
          "provider_response_code": "00",
          "provider": `${incomingPayload.auth.auth_provider}`,
          "errors": null,
          "error": null,
          "provider_response": {
            "reference": "000022200225154318222333334432",
            "payment_status": "Successful",
            "fulfillment_status": "Succesful",
            "transaction_final_amount": 20000,//in kobo
            "transaction_fee": 5000,//in kobo
            "narration": "My narration"
          }
        }
      }
      break;
    case "pay electricity":
      console.log("pay electricity")
      responseData = {
        "status": "Successful",
        "message": "Transaction processed successfully",
        "data": {
          "provider_response_code": "00",
          "provider": `${incomingPayload.auth.auth_provider}`,
          "errors": null,
          "error": null,
          "provider_response": {
            "reference": "000022200225154318222333334432",
            "payment_status": "Successful",
            "fulfillment_status": "Succesful",
            "transaction_final_amount": 20000,//in kobo
            "transaction_fee": 5000,//in kobo
            "narration": "My narration"
          }
        }
      }
      break;
    case "hold funds":
      console.log("hold funds")
      responseData = {
        "status": "Successful",
        "message": "Transaction processed successfully",
        "data": {
          "provider_response_code": "00",
          "provider": `${incomingPayload.auth.auth_provider}`,
          "errors": null,
          "error": null,
          "provider_response": {
            "provider_token": "22333055555",
            "token_expiry": "yyyy-MM-dd-hh-mm-ss",
            "hold_expiry": "yyyy-MM-dd-hh-mm-ss",
            "reference": "000022200225154318222333334432",
            "meta": {
              "field_key": "field_value",
              "field_key": "field_value"
            }
          }
        }
      }
      break;
    case "open wallet":
      console.log("open wallet")
      responseData = {
        "status": "Successful",
        "message": "Transaction processed successfully",
        "data": {
          "provider_response_code": "00",
          "provider": `${incomingPayload.auth.auth_provider}`,
          "errors": null,
          "error": null,
          "provider_response": {
            "account_number": "2233305555",
            "contract_code": null,
            "account_reference": "48214462006092",
            "account_name": "Akinkunmi Olunloye",
            "currency_code": "NGN",
            "customer_email": "akin@onepipe.io",
            "bank_ame": null,
            "bank_code": null,
            "account_type": null,
            "status": "ACTIVE",
            "created_on": "2020-04-05 23:08:01",
            "reference": "000022200225154318222333334432",
            "meta": {
              "field_key": "field_value",
              "field_key": "field_value"
            }
          }
        }
      }
      break;
    case "hold funds":
      console.log("hold funds")
      responseData = {
        "status": "Successful",
        "message": "Transaction processed successfully",
        "data": {
          "provider_response_code": "00",
          "provider": `${incomingPayload.auth.auth_provider}`,
          "errors": null,
          "error": null,
          "provider_response": {
            "hold_expiry": "yyyy-MM-dd-hh-mm-ss",
            "reference": "000022200225154318222333334432",
            "meta": {
              "field_key": "field_value",
              "field_key": "field_value"
            }
          }
        }
      }
      break;
    case "block wallet":
      console.log("block wallet")
      responseData = {
        "status": "Successful",
        "message": "Transaction processed successfully",
        "data": {
          "provider_response_code": "00",
          "provider": `${incomingPayload.auth.auth_provider}`,
          "errors": null,
          "error": null,
          "provider_response": {
            "hold_expiry": "yyyy-MM-dd-hh-mm-ss",
            "reference": "000022200225154318222333334432",
            "meta": {
              "field_key": "field_value",
              "field_key": "field_value"
            }
          }
        }
      }
      break;
    case "block account":
      console.log("block account")
      responseData = {
        "status": "Successful",
        "message": "Transaction processed successfully",
        "data": {
          "provider_response_code": "00",
          "provider": `${incomingPayload.auth.auth_provider}`,
          "errors": null,
          "error": null,
          "provider_response": {
            "hold_expiry": "yyyy-MM-dd-hh-mm-ss",
            "reference": "000022200225154318222333334432",
            "meta": {
              "field_key": "field_value",
              "field_key": "field_value"
            }
          }
        }
      }
      break;
    case "get loan status":
      console.log("get loan status")
      responseData = {
        "status": "Successful",
        "message": "Transaction processed successfully",
        "data": {
          "provider_response_code": "00",
          "provider": `${incomingPayload.auth.auth_provider}`,
          "errors": null,
          "error": null,
          "provider_response": {
            "loans": [
              {
                "account_number": "00999999000",
                "account_name": "Ola Waheed",
                "bank_name": "FBN",
                "bank_code": "011",
                "loan_date": "yyyy-MM-dd-hh-mm-ss",
                "loan_amount": 1000000,
                "loan_fees": 0,
                "loan_interest_percent": 15,
                "loan_interest_amount": 15,
                "loan_due_amount": 1000000,
                "loan_due_date": "yyyy-MM-dd-hh-mm-ss",
                "lender_code": "MNI",
                "lender_name": "Migo",
                "lender_terms": "Some content will go here",
                "lender_terms_url": "https://terms.html",
                "extended_data": {
                  "a_key": "a_value"
                }
              },
              {
                "account_number": "00999999000",
                "account_name": "Ola Waheed",
                "bank_name": "FBN",
                "bank_code": "011",
                "loan_date": "yyyy-MM-dd-hh-mm-ss",
                "loan_amount": 1000000,
                "loan_fees": 0,
                "loan_interest_percent": 15,
                "loan_interest_amount": 15,
                "loan_due_amount": 1000000,
                "loan_due_date": "yyyy-MM-dd-hh-mm-ss",
                "lender_code": "MNI",
                "lender_name": "Migo",
                "lender_terms": "Some content will go here",
                "lender_terms_url": "https://terms.html",
                "extended_data": {
                  "a_key": "a_value"
                }
              }
            ],
            "reference": "000022200225154318222333334432",
            "meta": {
              "field_key": "field_value",
            }
          }
        }
      }
      break;
    case "get a loan":
      console.log("get a loan")
      responseData = {
        "status": "OptionsDelivered",
        "message": "Transaction processed successfully",
        "data": {
          "provider_response_code": "00",
          "provider": `${incomingPayload.auth.auth_provider}`,
          "errors": null,
          "error": null,
          "provider_response": {
            "offers": [
              {
                "offer_id": "offer-019",
                "offer_expiry_date": "yyyy-MM-dd-hh-mm-ss",
                "loan_amount": 1000000,
                "loan_fees": 0,
                "loan_interest_percent": 15,
                "loan_interest_amount": 1500,
                "loan_due_amount": 1000000,
                "loan_due_date": "yyyy-MM-dd-hh-mm-ss",
                "lender_code": "MNI",
                "lender_product_code": "PRODUCT01",
                "lender_name": "Migo",
                "lender_terms": "Some content will go here",
                "lender_terms_url": "https://terms.html",
                "extended_data": {
                  "a_key": "a_value"
                }
              },
              {
                "offer_id": "offer-019",
                "offer_expiry_date": "yyyy-MM-dd-hh-mm-ss",
                "loan_amount": 1000000,
                "loan_fees": 0,
                "loan_interest_percent": 15,
                "loan_interest_amount": 1500,
                "loan_due_amount": 1000000,
                "loan_due_date": "yyyy-MM-dd-hh-mm-ss",
                "lender_code": "MNI",
                "lender_product_code": "PRODUCT01",
                "lender_name": "Migo",
                "lender_terms": "Some content will go here",
                "lender_terms_url": "https://terms.html",
                "extended_data": {
                  "a_key": "a_value"
                }
              }
            ],
            "consent_token": "65ggggs$eew",
            "consent_token_expiry": "yyyy-MM-dd-hh-mm-ss",
            "reference": "000022200225154318222333334432",
            "meta": {
              "field_key": "field_value",
              "field_key": "field_value"
            }
          }
        }
      }
      break;
    case "lookup id min":
      console.log("lookup id min")
      responseData = {
        "status": "Successful",
        "message": "Transaction processed successfully",
        "data": {
          "provider_response_code": "00",
          "provider": `${incomingPayload.auth.auth_provider}`,
          "errors": null,
          "error": null,
          "provider_response": {
            "first_name": true,
            "last_name": true,
            "reference": "000022200225154318222333334432",
            "meta": {
              "field_key": "field_value",
            }
          }
        }
      }
      break;
    case "lookup id max":
      console.log("lookup id max")
      responseData = {
        "status": "Successful",
        "message": "Transaction processed successfully",
        "data": {
          "provider_response_code": "00",
          "provider": `${incomingPayload.auth.auth_provider}`,
          "errors": null,
          "error": null,
          "provider_response": {
            "reference": "0987778887777766",
            "meta": {
              "field_01": "field_01_value",
            }
          }
        }
      }
      break;
    case "get credit score":
      console.log("get credit score")
      responseData = {
        "status": "Successful",
        "message": "Transaction processed successfully",
        "data": {
          "provider_response_code": "00",
          "provider": `${incomingPayload.auth.auth_provider}`,
          "errors": null,
          "error": null,
          "provider_response": {
            "loans_score": 9, //score assigned this user
            "scale_min": 0, //the minimum value on the score scale
            "scale_max": 10, //the maximum value on the score scale
            "offer_amount": 200000, //The maximum risk that the provider thinks is permissible
            "confidence": 90, //percentage of confidence attributted to this result
            "reference": "000022200225154318222333334432",
            "meta": {
              "field_key": "field_value"
            }
          }
        }
      }
      break;
    case "fund wallet":
      console.log("fund wallet")
      responseData = {
        "status": "Successful",
        "message": "Transaction processed successfully",
        "data": {
          "provider_response_code": "00",
          "provider": `${incomingPayload.auth.auth_provider}`,
          "errors": null,
          "error": null,
          "provider_response": {
            "payment_status": "Successful",
            "fulfillment_status": "Succesful",
            "transaction_final_amount": 20000,//in kobo
            "transaction_fee": 5000,//in kobo
            "narration": "My narration",
            "reference": "000022200225154318222333334432",
            "meta": {
              "field_key": "field_value"
            }
          }
        }
      }
      break;
    case "issue card":
      console.log("issue card")
      responseData = {
        "status": "Successful",
        "message": "Transaction processed successfully",
        "data": {
          "provider_response_code": "00",
          "provider": `${incomingPayload.auth.auth_provider}`,
          "errors": null,
          "error": null,
          "provider_response": {
            "card_no_masked": "506100xxxxxx4998",
            "card_sequence_no": "001",
            "card_brand": "Mastercard",
            "card_scheme_code": "001",
            "card_scheme_name": "Osun Students Card",
            "name_on_card": "Opeyemi O. Adamu",
            "address_line_1": "23 Ebinpejo Lane",
            "address_line_2": "Idumota",
            "address_city": "Lagos",
            "address_state": "Lagos",
            "address_postal_code": "23401",
            "address_country_code": "NGN",
            "card_currency_code": "566",
            "issuer_cbn_code": "43",
            "otp_setup_response": "9000",
            "secure": `TripleDES.encrypt("{card.Pan};{card.Cvv};{card.Expdate};{card.Pin}",secretKey)`,
            "reference": "000022200225154318222333334432",
            "meta": {
              "field_key": "field_value"
            }
          }
        }
      }
      break;
    case "change pin":
      console.log("change pin")
      responseData = {
        "status": "Successful",
        "message": "Transaction processed successfully",
        "data": {
          "provider_response_code": "00",
          "provider": `${incomingPayload.auth.auth_provider}`,
          "errors": null,
          "error": null,
          "provider_response": {
            "reference": "000022200225154318222333334432",
            "meta": {
              "field_key": "field_value"
            }
          }
        }
      }
      break;
    case "reissue pin":
      console.log("reissue pin")
      responseData = {
        "status": "Successful",
        "message": "Transaction processed successfully",
        "data": {
          "provider_response_code": "00",
          "provider": `${incomingPayload.auth.auth_provider}`,
          "errors": null,
          "error": null,
          "provider_response": {
            "new_pin": "1234",
            "pin_offset": "1245",
            "reference": "000022200225154318222333334432",
            "meta": {
              "field_key": "field_value",
              "field_key": "field_value"
            }
          }
        }
      }
      break;
    default:
      console.log("I'm not calling anything")
      break;
  }

  encryptedResponse = encrypt(
    `${process.env.enc_key}`,
    JSON.stringify(responseData)
  );
  res.json({
    data: encryptedResponse
  });
});
module.exports = Router;