const { encrypt, decrypt } = require('../onepipe-crypto');
const decode = (enc_key,data)=>{
    let decryptedPayload = decrypt(`${enc_key}`,data)
    let filtered = decryptedPayload.split("").filter(function(e) {
        return e != "\u0000";
      });
      let incomingPayload = JSON.parse(filtered.join(""));
      return incomingPayload;
}


module.exports = decode;